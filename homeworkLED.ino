#include <Ticker.h>

Ticker timer250;
Ticker timer500;
Ticker timer1000;

#define R_LED D0
#define SW_R D3

#define Y_LED D1
#define SW_Y D5

#define G_LED D2
#define SW_G D6

volatile bool timer1000check = false;

void time1000() {
  timer1000check = true;
}

volatile bool timer500check = false;

void time500() {
  timer500check = true;
}

volatile bool timer250check = false;

void time250() {
  timer250check = true;
}

volatile bool red = false;

ICACHE_RAM_ATTR void redInterrupt() {
  red = true;
}

volatile bool Yellow = false;

ICACHE_RAM_ATTR void YellowInterrupt() {
  Yellow = true;
}

volatile bool Green = false;
volatile bool stopTimer500 = false;

ICACHE_RAM_ATTR void GreenInterrupt() {
  Green = true;
  stopTimer500 = true;
}

void setup() {
  Serial.begin(9600);
  Serial.println("Hello World");
  pinMode(R_LED, OUTPUT);
  pinMode(Y_LED, OUTPUT);
  pinMode(G_LED, OUTPUT);

  pinMode(SW_R, INPUT_PULLUP);
  pinMode(SW_Y, INPUT_PULLUP);
  pinMode(SW_G, INPUT_PULLUP);

  digitalWrite(R_LED, 0);
  digitalWrite(Y_LED, 1);
  digitalWrite(G_LED, 0);

  attachInterrupt(digitalPinToInterrupt(SW_R), redInterrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(SW_Y), YellowInterrupt, RISING);
  attachInterrupt(digitalPinToInterrupt(SW_G), GreenInterrupt, RISING);
  timer250.attach(0.25, time250);
  timer500.attach(0.5, time500);
  timer1000.attach(1, time1000);
  
}


int loop1000 = 1;
int greenmode = 0;

int loop500 = 1;
int redmode = 0;

int loop250 = 1;
int yellowmode = 0;
int loopGreen = 0;
int loopYellow = 0;
int loopRed = 0;



void loop() {
  if (red) {
    Serial.println("RED SWITCH!");
    redmode = 1;
    red = false;
    timer500.attach(0.5, time500);
    timer1000.detach();
    timer250.detach();
  }
  if (timer500check && redmode == 1) {
    Serial.println("TIMER 500");
    if (loop500 == 1) {
      digitalWrite(Y_LED, 1);
      digitalWrite(R_LED, 0);
      digitalWrite(G_LED, 0);
      loop500 = 2;
    } else if (loop500 == 2) {
      digitalWrite(Y_LED, 0);
      digitalWrite(R_LED, 1);
      digitalWrite(G_LED, 0);
      loop500 = 3;
    } else if (loop500 == 3) {
      digitalWrite(Y_LED, 0);
      digitalWrite(R_LED, 0);
      digitalWrite(G_LED, 1);
      loop500 = 1;
    }
    timer500check = false;
  }

  if (Green) {
    Serial.println("Green SWITCH!");
    greenmode = 1;
    Green = false;
    timer500.detach();
    timer250.detach();
    timer1000.attach(1, time1000);
  }
  if (timer1000check && greenmode == 1) {
   
    if (loop1000 == 1) {
      digitalWrite(Y_LED, 1);
      digitalWrite(R_LED, 0);
      digitalWrite(G_LED, 0);
      loop1000 = 2;
    } else if (loop1000 == 2) {
      digitalWrite(Y_LED, 0);
      digitalWrite(R_LED, 0);
      digitalWrite(G_LED, 1);
      loop1000 = 3;
    } else if (loop1000 == 3) {
      digitalWrite(Y_LED, 0);
      digitalWrite(R_LED, 1);
      digitalWrite(G_LED, 0);
      loop1000 = 1;
    }
    timer1000check = false;
  }
  if (Yellow) {
    Serial.println("Yellow SWITCH!");
    yellowmode = 1;
    Yellow = false;
    loopGreen = 1;
    loopRed = 1;
    loopYellow = 1;
    timer250.attach(0.25, time250);
    timer500.detach();
    timer1000.detach();
  }

  if (timer250check && yellowmode == 1) {
   
    if (loop250 == 1) {
      if (digitalRead(G_LED) == HIGH && loopGreen == 1) {
        digitalWrite(G_LED, LOW);
        loopGreen = 2;
      } else if (loopGreen == 2) {
        digitalWrite(G_LED, HIGH);
        loopGreen = 1;
      } else if (digitalRead(R_LED) == HIGH && loopRed == 1) {
        digitalWrite(R_LED, LOW);
        loopRed = 2;
      } else if (loopRed == 2) {
        digitalWrite(R_LED, HIGH);
        loopRed = 1;
      } else if (digitalRead(Y_LED) == HIGH && loopYellow == 1) {
        digitalWrite(Y_LED, LOW);
        loopYellow = 2;
      } else if (loopYellow == 2) {
        digitalWrite(Y_LED, HIGH);
        loopYellow = 1;
      }
    }
    loop250 = 1;
    timer250check = false;
  }
}